@extends('adminlte.master')

@section('content')
<div class="ml-3 mt-3">
    <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">Edit Cast {{$cast->id}}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="/cast/{{$cast->id}}" method="POST">
            @csrf
            @method('PUT')
          <div class="card-body">
            <div class="form-group">
              <label for="nama">Nama</label>
              <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Lengkap" value="{{ old('nama', $cast->nama)}}">
              @error('nama')
                  <div class="alert alert-danger">{{ $message}}</div>
              @enderror
            </div>
            <div class="form-group">
                <label for="umur">Umur</label>
                <input type="string" class="form-control" id="umur" name="umur" placeholder="Umur" value="{{old('umur', $cast->umur)}}">
                @error('umur')
                  <div class="alert alert-danger">{{ $message}}</div>
              @enderror
            </div>
            <div class="form-group">
                <label for="bio">Bio</label>
                <input type="Text" class="form-control" id="bio" name="bio" placeholder="Bio" value="{{old('bio', $cast->bio)}}>
                @error('bio')
                  <div class="alert alert-danger">{{ $message}}</div>
              @enderror
            </div>
          </div>
          <!-- /.card-body -->
    
          <div class="card-footer">
            <button type="submit" class="btn btn-primary">Update</button>
          </div>
        </form>
      </div>
</div>

@endsection