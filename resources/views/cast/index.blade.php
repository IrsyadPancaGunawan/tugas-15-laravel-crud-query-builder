@extends('adminlte.master')

@section('content')
    <div class="ml-3 mt-3">
        <div class="card">
            <div class="card-header">
              <h3 class="card-title">Cast Table</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th style="width: 10px">#</th>
                    <th>Nama</th>
                    <th>Umur</th>
                    <th>Bio</th>
                    <th style="width: 40px">Action</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($cast as $key => $cast)
                      <tr>
                          <td>{{ $key +1 }} </td>
                          <td>{{ $cast->nama }}</td>
                          <td>{{ $cast->umur }}</td>
                          <td>{{ $cast->body }}</td>
                          <td style="display: flex"> 
                              <a href="/cast/{{$cast->id}}" class="btn btn-info btn-sm">Show</a>
                              <a href="/cast/{{$cast->id}}/edit" class="btn btn-default btn-sm">Edit</a>
                              <form action="/cast/{{$cast->id}}" method="post">
                                @csrf
                                @method('DELETE')
                                <input type="submit" value="delete" class="btn btn-danger btn-sm">
                              </form>
                          </td>
                      </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
            {{-- <div class="card-footer clearfix">
              <ul class="pagination pagination-sm m-0 float-right">
                <li class="page-item"><a class="page-link" href="#">&laquo;</a></li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item"><a class="page-link" href="#">&raquo;</a></li>
              </ul>
            </div> --}}
          </div>
    </div>
@endsection