<!DOCTYPE html>
<html>
<body>

<h2>Buat Account Baru!</h2>
<h4>Sign Up Form</h4>

<form action="/welcome" methods="POST">
    @csrf
    <label>First name:</label> <br><br>
    <input type="text" name="namaAwal"> <br><br>

    <label>Last name:</label> <br><br>
    <input type="text" name="namaAkhir"> <br><br>

    <label>Gender:</label> <br><br>
    <input type="radio">Male <br>
    <input type="radio">Female <br>
    <input type="radio">Other <br><br>

    <label>Nasionality:</label> <br><br>
    <select name="Negara">
        <option value="Indonesia">Indonesia</option>
        <option value="Malaysia">Malaysia</option>
        <option value="Singapura">Singapura</option>
        <option value="Jepang">Jepang</option>
    </select> <br><br>

    <label>Language Spoken:</label> <br><br>
    <input type="checkbox" name="skill">Bahasa Indonesia <br>
    <input type="checkbox" name="skill">English <br>
    <input type="checkbox" name="skill">Other <br><br>

    <label>Bio:</label> <br><br>
    <textarea name="message" cols="30" rows="10"></textarea>
    <br><br>

    <button href="/welcome">Sign Up</button>
</form>

</body>
</html>